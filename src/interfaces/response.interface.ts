export interface SearchResponse {
  hits: Hits[];
  nbHits: number;
  page: number;
  nbPages: number;
  hitsPerPage: number;
  exhaustiveNbHits: boolean;
  exhaustiveTypo: boolean;
  query: string;
  params: string;
  processingTimeMS: number;
}

export interface Hits {
  created_at: string;
  title?: string;
  url?: string;
  author: string;
  points?: number;
  story_text?: string;
  comment_text?: string;
  num_comments?: number;
  story_id?: number;
  story_title?: string;
  story_url?: string;
  parent_id?: number;
  created_at_i?: number;
  _tags: string[];
  objectID: string;
  _highlightResult: HighlightResult;
}

export interface HighlightResult {
  author: Author;
  comment_text: CommentText;
  story_title: StoryTitle;
  story_url: StoryUrl;
}

export interface Author {
  value: string;
  matchLevel: string;
  matchedWords: string[];
}

export interface CommentText extends Author {
  fullyHighlighted: false;
}

export type StoryTitle = Author;

export type StoryUrl = Author;

export interface PaginatedResponse {
  articles: any;
  totalPages: number;
}
