import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ServerController } from './server/server.controller';
import { ServerService } from './server/server.service';
import { PrismaService } from './prisma.service';
import { ScheduleModule } from '@nestjs/schedule';
import { TasksService } from './task/task.service';
import { TasksModule } from './task/task.module';
import { ServerModule } from './server/server.module';

@Module({
  imports: [
    HttpModule.registerAsync({
      useFactory: () => ({
        timeout: 5000,
        maxRedirects: 5,
      }),
    }),
    ScheduleModule.forRoot(),
    TasksModule,
    ServerModule,
  ],
  controllers: [AppController, ServerController],
  providers: [AppService, ServerService, PrismaService, TasksService],
})
export class AppModule {}
