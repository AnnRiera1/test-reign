import { Test, TestingModule } from '@nestjs/testing';
import { TasksService } from './task.service';
import { ServerService } from '../server/server.service';

describe('TaskService', () => {
  let service: TasksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: TasksService,
          useValue: {},
        },
        {
          provide: ServerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TasksService>(TasksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
