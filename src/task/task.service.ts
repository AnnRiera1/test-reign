import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { ServerService } from '../server/server.service';
import { hackNewsUrl } from '../constants/index';

@Injectable()
export class TasksService {
  constructor(private serverService: ServerService) {}
  private readonly logger = new Logger(TasksService.name);

  @Cron(CronExpression.EVERY_HOUR, {
    name: 'Updating database',
  })
  async handleCron() {
    const firstPage = await this.serverService.retrieveData(hackNewsUrl);
    const nbHits = await this.serverService.getNbHits();

    if (firstPage.nbHits > nbHits.nb_hits) {
      const totalCount = firstPage.nbHits - nbHits.nb_hits;
      this.logger.debug(`There was ${totalCount} new articles found.`);
      const newHits = await this.serverService.retrieveData(
        `${hackNewsUrl}&hitsPerPage=${totalCount}`,
      );
      const articles = await this.serverService.createArticles(newHits.hits);
      await this.serverService.addNbHits(newHits.nbHits);
      this.logger.debug(`${articles} new articles were created successfully.`);
    } else {
      this.logger.debug('There is no new articles to fetch.');
    }
  }
}
