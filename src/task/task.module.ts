import { Module } from '@nestjs/common';
import { TasksService } from './task.service';
import { ServerModule } from 'src/server/server.module';

@Module({
  imports: [ServerModule],
  providers: [TasksService],
})
export class TasksModule {}
