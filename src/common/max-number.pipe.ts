import { Injectable, PipeTransform, BadRequestException } from '@nestjs/common';

@Injectable()
export class MaxNumberPipe implements PipeTransform {
  transform(value: number) {
    if (value > 5) {
      throw new BadRequestException(
        'per_page param must be less than or equal to 5',
      );
    }
    return value;
  }
}
