import { NotZeroPipe } from './not-zero.pipe';

describe('NotZeroPipe', () => {
  it('should be defined', () => {
    expect(new NotZeroPipe()).toBeDefined();
  });
});
