import { Injectable, PipeTransform, BadRequestException } from '@nestjs/common';

@Injectable()
export class NotZeroPipe implements PipeTransform {
  transform(value: number) {
    if (value <= 0) {
      throw new BadRequestException('Param must be greater than zero');
    }
    return value;
  }
}
