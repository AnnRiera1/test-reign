import {
  Controller,
  Get,
  Delete,
  HttpStatus,
  HttpCode,
  Query,
  Param,
  Response,
  ParseIntPipe,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiTags,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import { PaginatedResponse } from '../interfaces/response.interface';
import { ServerService } from './server.service';
import { NotZeroPipe } from '../common/not-zero.pipe';
import { MaxNumberPipe } from '../common/max-number.pipe';
import { NbHits } from '@prisma/client';

@ApiTags('server')
@Controller('server')
export class ServerController {
  constructor(private serverService: ServerService) {}

  @Get('search')
  @ApiOperation({ summary: 'Search articles by filters' })
  @ApiOkResponse({
    description: 'The request was successfull',
  })
  @ApiBadRequestResponse({ description: 'One or more params are invalid' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @HttpCode(HttpStatus.OK)
  async searchArticles(
    @Query('page', ParseIntPipe, NotZeroPipe) page: number,
    @Query('per_page', ParseIntPipe, NotZeroPipe, MaxNumberPipe)
    perPage: number,
    @Query('author') author?: string,
    @Query('_tags') tags?: string,
    @Query('title') title?: string,
  ): Promise<PaginatedResponse> {
    return await this.serverService.searchArticles(
      page,
      perPage,
      author,
      tags,
      title,
    );
  }

  @Get('fill')
  @ApiOperation({ summary: 'Fill the database with a request of the API' })
  @ApiOkResponse({
    description: 'The request was successfull',
  })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @HttpCode(HttpStatus.OK)
  async fillDatabase(@Response() res): Promise<void> {
    const all = await this.serverService.retrieveAllPages();
    const count = await this.serverService.createArticles(all);
    return res.send({
      message: `Database was filled successfully with ${count} records.`,
    });
  }

  @Delete('remove/one/:id')
  @ApiOperation({ summary: 'Remove one article by id' })
  @ApiOkResponse({
    description: 'The request was successfull',
  })
  @ApiBadRequestResponse({ description: 'Id is invalid' })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @HttpCode(HttpStatus.ACCEPTED)
  async removeOne(
    @Param('id', ParseIntPipe, NotZeroPipe) id: number,
    @Response() res,
  ): Promise<void> {
    const removed = await this.serverService.removeOne(id);
    return res.send({
      message: `Article with id: ${removed.id} was deleted successfully.`,
    });
  }

  @Delete('remove/many/:authorName')
  @ApiOperation({ summary: 'Remove one or more articles by author name' })
  @ApiOkResponse({
    description: 'The request was successfull',
  })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @HttpCode(HttpStatus.ACCEPTED)
  async removeMany(
    @Param('authorName') authorName: string,
    @Response() res,
  ): Promise<void> {
    const removed = await this.serverService.removeMany(authorName);
    return res.send({
      message: `${removed} articles were removed successfully.`,
    });
  }

  @Get('nbhits')
  @ApiOperation({
    summary: 'Return the last number of hits stored in the database',
  })
  @ApiOkResponse({
    description: 'The request was successfull',
  })
  @ApiInternalServerErrorResponse({ description: 'Something went wrong' })
  @HttpCode(HttpStatus.OK)
  async getLastNbHit(): Promise<NbHits> {
    return await this.serverService.getNbHits();
  }
}
