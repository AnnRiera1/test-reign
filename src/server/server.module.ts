import { Module } from '@nestjs/common';
import { ServerController } from './server.controller';
import { ServerService } from './server.service';
import { HttpModule } from '@nestjs/axios';
import { PrismaService } from '../prisma.service';

@Module({
  imports: [HttpModule],
  controllers: [ServerController],
  providers: [ServerService, PrismaService],
  exports: [ServerService],
})
export class ServerModule {}
