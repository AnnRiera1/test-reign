import { Test, TestingModule } from '@nestjs/testing';
import { ServerService } from './server.service';
import { HttpService } from '@nestjs/axios';
import { PrismaService } from '../prisma.service';

describe('ServerService', () => {
  let service: ServerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        {
          provide: ServerService,
          useValue: {
            retrieveData: jest.fn().mockReturnValue({
              hits: [
                {
                  created_at: '2022-03-28T22:41:46.000Z',
                  title: null,
                  url: null,
                  author: 'AtlasBarfed',
                  points: null,
                  story_text: null,
                  comment_text:
                    'What are you using to scale the document storage across nodes? Is it the usual master-slave stuff?',
                  num_comments: null,
                  story_id: 30835444,
                  story_title:
                    'Writing a document database from scratch in Go: Lucene-like filters and indexes',
                  story_url: 'https://notes.eatonphil.com/documentdb.html',
                  parent_id: 30835444,
                  created_at_i: 1648507306,
                  _tags: ['comment', 'author_AtlasBarfed', 'story_30835444'],
                  objectID: '30836474',
                  _highlightResult: {
                    author: {
                      value: 'AtlasBarfed',
                      matchLeve: 'none',
                      matchedWords: [],
                    },
                    comment_text: {
                      value:
                        'What are you using to scale the document storage across <em>nodes</em>? Is it the usual master-slave stuff?',
                      matchLevel: 'full',
                      fullyHighlighted: false,
                      matchedWords: ['nodejs'],
                    },
                    story_title: {
                      value:
                        'Writing a document database from scratch in Go: Lucene-like filters and indexes',
                      matchLeve: 'none',
                      matchedWord: [],
                    },
                    story_url: {
                      value: 'https://notes.eatonphil.com/documentdb.html',
                      matchLevel: 'none',
                      matchedWord: [],
                    },
                  },
                },
              ],
              nbHit: 22537,
              page: 0,
              nbPages: 50,
              hitsPerPage: 20,
              exhaustiveNbHits: true,
              exhaustiveTypo: true,
              query: 'nodejs',
              params:
                'advancedSyntax=true&analytics=true&analyticsTags=backend&query=nodejs',
              processingTimeMS: 10,
            }),
            retrieveAllPages: jest.fn().mockReturnValue({
              hits: [
                {
                  created_at: '2022-03-28T22:41:46.000Z',
                  title: null,
                  url: null,
                  author: 'AtlasBarfed',
                  points: null,
                  story_text: null,
                  comment_text:
                    'What are you using to scale the document storage across nodes? Is it the usual master-slave stuff?',
                  num_comments: null,
                  story_id: 30835444,
                  story_title:
                    'Writing a document database from scratch in Go: Lucene-like filters and indexes',
                  story_url: 'https://notes.eatonphil.com/documentdb.html',
                  parent_id: 30835444,
                  created_at_i: 1648507306,
                  _tags: ['comment', 'author_AtlasBarfed', 'story_30835444'],
                  objectID: '30836474',
                  _highlightResult: {
                    author: {
                      value: 'AtlasBarfed',
                      matchLeve: 'none',
                      matchedWords: [],
                    },
                    comment_text: {
                      value:
                        'What are you using to scale the document storage across <em>nodes</em>? Is it the usual master-slave stuff?',
                      matchLevel: 'full',
                      fullyHighlighted: false,
                      matchedWords: ['nodejs'],
                    },
                    story_title: {
                      value:
                        'Writing a document database from scratch in Go: Lucene-like filters and indexes',
                      matchLeve: 'none',
                      matchedWord: [],
                    },
                    story_url: {
                      value: 'https://notes.eatonphil.com/documentdb.html',
                      matchLevel: 'none',
                      matchedWord: [],
                    },
                  },
                },
              ],
              nbHit: 22537,
              page: 0,
              nbPages: 50,
              hitsPerPage: 20,
              exhaustiveNbHits: true,
              exhaustiveTypo: true,
              query: 'nodejs',
              params:
                'advancedSyntax=true&analytics=true&analyticsTags=backend&query=nodejs',
              processingTimeMS: 10,
            }),
            createArticles: jest.fn().mockReturnValue(1),
            createAuthors: jest.fn().mockReturnValue(2),
            removeOne: jest.fn().mockReturnValue({
              id: 1,
              title: null,
              url: null,
              author: 'jitl',
              points: null,
              story_text: null,
              comment_text:
                'Wow, this is a great resource! I have been dealing with a lot of these issues with a JS sandbox library I am building quickjs-emecripten. I want to offer a ton of different build variants of my core WASM file. Here’s what I tried: <a href="https:&#x2F;&#x2F;github.com&#x2F;justjake&#x2F;quickjs-emscripten&#x2F;blob&#x2F;master&#x2F;ts&#x2F;variants.ts" rel="nofollow">https:&#x2F;&#x2F;github.com&#x2F;justjake&#x2F;quickjs-emscripten&#x2F;blob&#x2F;master&#x2F;t...</a><p>This approach works create for NodeJS, but once I ran a test bundle I found that Webpack (and bundlephobia) included all the base64 “release” variants instead of lazy-loading the import statements. Bummer. I assumed this because Typescript on its own compiled import to Promise.resolve(require(…)), so it’s good to know that most bundlers will STILL get this wrong even if I’m emitting ES6 module import syntax. Yikes! I need to bite the bullet and start using Rollup to emit a slew of separate entry points. Oy veh.<p>Anyways A+++ would read again. This will save me 4-5 days of work stubbing my toe on bundlers and build system which is the Least Fun  part of JS work.',
              tags: ['comment', 'author_jitl', 'story_30829170'],
              num_comments: null,
              object_id: '30830272',
              highlight_result: {
                author: {
                  value: 'jitl',
                  matchLevel: 'none',
                  matchedWords: [],
                },
                story_url: {
                  value:
                    'https://nickb.dev/blog/recommendations-when-publishing-a-wasm-library',
                  matchLevel: 'none',
                  matchedWords: [],
                },
                story_title: {
                  value: 'Recommendations when publishing a WASM library',
                  matchLevel: 'none',
                  matchedWords: [],
                },
                comment_text: {
                  value:
                    'Wow, this is a great resource! I have been dealing with a lot of these issues with a JS sandbox library I am building quickjs-emecripten. I want to offer a ton of different build variants of my core WASM file. Here’s what I tried: <a href="https://github.com/justjake/quickjs-emscripten/blob/master/ts/variants.ts" rel="nofollow">https://github.com/justjake/quickjs-emscripten/blob/master/t...</a><p>This approach works create for <em>NodeJS</em>, but once I ran a test bundle I found that Webpack (and bundlephobia) included all the base64 “release” variants instead of lazy-loading the import statements. Bummer. I assumed this because Typescript on its own compiled import to Promise.resolve(require(…)), so it’s good to know that most bundlers will STILL get this wrong even if I’m emitting ES6 module import syntax. Yikes! I need to bite the bullet and start using Rollup to emit a slew of separate entry points. Oy veh.<p>Anyways A+++ would read again. This will save me 4-5 days of work stubbing my toe on bundlers and build system which is the Least Fun  part of JS work.',
                  matchLevel: 'full',
                  matchedWords: ['nodejs'],
                  fullyHighlighted: false,
                },
              },
              created_at: '2022-03-28T13:23:59.000Z',
              story_id: 30829170,
              story_title: 'Recommendations when publishing a WASM library',
              story_url:
                'https://nickb.dev/blog/recommendations-when-publishing-a-wasm-library',
              parent_id: 30829170,
              created_at_i: 1648473839,
              author_id: 1,
              Author: {
                id: 1,
                username: 'jitl',
                match_level: 'none',
                matched_words: [],
              },
            }),
            removeMany: jest.fn().mockReturnValue(4),
            addNbHits: jest.fn().mockReturnValue({}),
            getNbHits: jest.fn().mockReturnValue({
              id: 8,
              nb_hits: 22537,
              created_at: '2022-03-28T23:59:02.040Z',
            }),
          },
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: PrismaService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ServerService>(ServerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('retrieveData()', () => {
    it('should retrieve one page from API', async () => {
      const page = await service.retrieveData('test');
      expect(service.retrieveData).toHaveBeenCalled();
      expect(page).toBeDefined();
      expect(typeof page).toBe('object');
    });
  });

  describe('retrieveAllPages()', () => {
    it('should fetch all pages from API', async () => {
      const all = await service.retrieveAllPages();
      expect(service.retrieveAllPages).toHaveBeenCalled();
      expect(all).toBeDefined();
      expect(typeof all).toBe('object');
    });
  });

  describe('createAuthors()', () => {
    it('should create every fetched article into the database', async () => {
      const author = await service.createAuthors({
        value: 'jitl',
        matchLevel: 'none',
        matchedWords: [],
      });
      expect(author).toBeDefined();
      expect(typeof author).toBe('number');
      expect(author).toEqual(2);
    });
  });

  describe('createArticles()', () => {
    it('should create every fetched article into the database', async () => {
      const all = await service.retrieveAllPages();
      const articles = await service.createArticles(all);
      expect(all).toBeDefined();
      expect(articles).toBeDefined();
      expect(typeof all).toBe('object');
      expect(typeof articles).toBe('number');
    });
  });

  describe('removeOne()', () => {
    it('should physically delete one article from database', async () => {
      const removed = await service.removeOne(1);
      expect(removed).toBeDefined();
      expect(typeof removed).toBe('object');
      expect(removed.id).toBe(1);
    });
  });

  describe('removeMany()', () => {
    it('should physically delete one or more articles from database', async () => {
      const removed = await service.removeMany('cute_boi');
      expect(removed).toBeDefined();
      expect(typeof removed).toBe('number');
      expect(removed).toBe(4);
    });
  });

  describe('addNbHits()', () => {
    it('should physically delete one or more articles from database', async () => {
      jest.spyOn(service, 'addNbHits').getMockImplementation();
    });
  });

  describe('getNbHits()', () => {
    it('should return the last nb hit added in the database', async () => {
      const hits = await service.getNbHits();
      expect(hits).toBeDefined();
      expect(typeof hits).toBe('object');
      expect(hits.nb_hits).toBe(22537);
    });
  });
});
