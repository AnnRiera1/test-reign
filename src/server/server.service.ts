import { HttpService } from '@nestjs/axios';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { Article, Prisma, NbHits } from '@prisma/client';
import { hackNewsUrl } from '../constants/index';
import { lastValueFrom } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  SearchResponse,
  Hits,
  Author,
  PaginatedResponse,
} from '../interfaces/response.interface';

@Injectable()
export class ServerService {
  constructor(
    private httpService: HttpService,
    private prisma: PrismaService,
  ) {}

  async delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async retrieveData(url: string): Promise<SearchResponse> {
    try {
      const page: SearchResponse = await lastValueFrom(
        this.httpService
          .get(url, {
            headers: {
              Accept: 'application/json',
            },
          })
          .pipe(map((response) => response.data)),
      );

      return page;
    } catch (error) {
      console.log(error);
      throw new Error(error);
    }
  }

  async retrieveAllPages(): Promise<Hits[]> {
    try {
      const firstPage: SearchResponse = await this.retrieveData(hackNewsUrl);
      if (firstPage.nbPages > 0) {
        const concatenatedArray: Hits[] = [...firstPage.hits];
        for (let index = 1; index <= firstPage.nbPages; index++) {
          await this.delay(2000);
          const data: SearchResponse = await this.retrieveData(
            `${hackNewsUrl}&page=${index}`,
          );
          concatenatedArray.push(...data.hits);
        }
        await this.addNbHits(firstPage.nbHits);
        return concatenatedArray;
      }
      await this.addNbHits(firstPage.nbHits);
      return firstPage.hits;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async searchArticles(
    page: number,
    perPage: number,
    author?: string,
    tags?: string,
    title?: string,
  ): Promise<PaginatedResponse> {
    try {
      const where: any = {
        AND: [],
      };

      const numSkip = perPage * page - perPage;

      if (author) {
        where.AND.push({
          author: {
            contains: author,
            mode: 'insensitive',
          },
        });
      }

      if (tags && tags.length > 0) {
        where.AND.push({
          tags: {
            hasSome: tags.split(','),
          },
        });
      }

      if (title) {
        where.AND.push({
          title: {
            contains: title,
            mode: 'insensitive',
          },
        });
      }

      const [articles, count] = await Promise.all([
        this.prisma.article.findMany({
          skip: numSkip,
          take: perPage,
          where,
          include: {
            Author: true,
          },
        }),
        this.prisma.article.count({
          where,
        }),
      ]);

      const totalCount = Math.ceil(count / perPage);
      return {
        articles: articles,
        totalPages: totalCount,
      };
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async createAuthors(author: Author): Promise<number> {
    try {
      const newAuthor = await this.prisma.author.create({
        data: {
          username: author.value,
          match_level: author.matchLevel,
          matched_words: author.matchedWords,
        },
      });

      return newAuthor.id;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async createArticles(all: Hits[]) {
    try {
      for (const article of all) {
        const author_id = await this.createAuthors(
          article._highlightResult.author,
        );

        await this.prisma.article.create({
          data: {
            title: article.title,
            url: article.url,
            author: article.author,
            points: article.points,
            story_text: article.story_text,
            comment_text: article.comment_text,
            tags: article._tags as Prisma.JsonArray,
            num_comments: article.num_comments,
            story_id: article.story_id,
            story_title: article.story_title,
            story_url: article.story_url,
            parent_id: article.parent_id,
            created_at_i: article.created_at_i,
            created_at: article.created_at,
            object_id: article.objectID,
            author_id: author_id,
            highlight_result:
              article._highlightResult as unknown as Prisma.JsonObject,
          },
        });
      }

      return all.length;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async removeOne(articleId: number): Promise<Article> {
    try {
      const removedItem = await this.prisma.article.delete({
        where: {
          id: articleId,
        },
      });
      return removedItem;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async removeMany(username: string): Promise<number> {
    try {
      const removedItems = await this.prisma.article.deleteMany({
        where: {
          author: username,
        },
      });
      return removedItems.count;
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async addNbHits(nbHits: number): Promise<void> {
    try {
      await this.prisma.nbHits.create({
        data: {
          nb_hits: nbHits,
          created_at: new Date(),
        },
      });
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async getNbHits(): Promise<NbHits> {
    try {
      return await this.prisma.nbHits.findFirst({
        orderBy: {
          created_at: 'desc',
        },
      });
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }
}
