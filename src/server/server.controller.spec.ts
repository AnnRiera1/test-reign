import { Test, TestingModule } from '@nestjs/testing';
import { ServerController } from './server.controller';
import { ServerService } from './server.service';

describe('ServerController', () => {
  let controller: ServerController;
  let serverService: ServerService;

  const res = {
    send: function (obj: any) {
      return obj;
    },
    json: function (err) {
      console.log('\n : ' + err);
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServerController],
      providers: [
        {
          provide: ServerService,
          useValue: {
            searchArticles: jest.fn().mockReturnValue({
              articles: [
                {
                  id: 2,
                  title: null,
                  url: null,
                  author: 'sho_hn',
                  points: null,
                  story_text: null,
                  comment_text:
                    'To add to this: A much more common Linux equivalent to this is &quot;making Yocto Linux recompile your OS image&quot;. Yocto is not the only option in this space, but perhaps the closest embedded Linux development has to a standard option.<p>Yocto is not entirely unlike Gentoo - recipes describe components of the system, which is built from source - but with the addition of a fairly sophisticated caching scheme, where the input to a recipe is hashed and the hash used to store the outcome, which is then reused in future builds (and the cache can be shared by different builders) unless the input changes. The other key feature of Yocto is that the system is composable in layers, where upper layers add, extend or override recipes from the lower layers. Layers can and are provided by different parties (e.g. BSP layers from HW vendors or their SW partners).<p>Yocto Linux is used by projects which need to be able to guarantee that they can bootstrap their OS build, customize the build (e.g. filter out use of certain licenses or use a specific toolchain) and to manage SW supply chain (the layer idea, and then in a business contact something like RASIC for the layers).<p>To sum it up, &quot;rebuilding the entire Linux OS w&#x2F; caching&quot; is absolutely the norm in embedded dev, as is having a compiler farm doing this hooked into your CI. Edge nodes like developer laptops then access the CI build farm&#x27;s caches to make a local build bearable, with the caveat that you don&#x27;t get incremental builds at the component level this way, so usually you still want to either dev separately against an SDK (or reuse the build root) or at least keep your components small and modular enough to not make it painful.<p>Automotive, network equipment (e.g. routers), stage&#x2F;production equipment (mixers and other networked A&#x2F;V gear, etc.) and many other parts of the SW world work this way.<p>Hacker News is mostly exposed to web development and desktop Linux&#x2F;mobile app development, which are pretty different. Indeed, perhaps the most surprising thing is how different desktop Linux development is from embedded Linux development and how little cross-pollination between these communities is taking place.<p>If you develop your for desktop Linux, the distro on your box also serves as its SDK - you just install a bunch of -dev packages from your package manager and build against the host. Or perhaps a Docker image as build env in some cases. But you generally never rebuild&#x2F;bootstrap the OS, which in embedded is the primary unit of work (with mitigations such as caching).<p>One side-effect of this is that embedded systems and desktop systems tend to approach updates&#x2F;OTA differently. In the package&#x2F;binary-based systems, OTAs come in via the package manager. Embedded systems historically tend to go for full system image updates with again some mitigations such as binary deltas, and then A&#x2F;B partition update schemes. Or a partitioning of the update content that is orthogonal to the partitioning that goes into the OS image build. Lately there&#x27;s a trend for seperating applications out into container images that get deployed seperately from the base OS image, and thoughts about containers that can move between embedded devices on the edge and the cloud infra in the back.',
                  tags: ['comment', 'author_sho_hn', 'story_30827210'],
                  num_comments: null,
                  object_id: '30830122',
                  highlight_result: {
                    author: {
                      value: 'sho_hn',
                      matchLevel: 'none',
                      matchedWords: [],
                    },
                    story_url: {
                      value:
                        'https://fuchsia.dev/fuchsia-src/development/build/build_workstation',
                      matchLevel: 'none',
                      matchedWords: [],
                    },
                    story_title: {
                      value: 'Fuchsia Workstation',
                      matchLevel: 'none',
                      matchedWords: [],
                    },
                    comment_text: {
                      value:
                        "To add to this: A much more common Linux equivalent to this is &quot;making Yocto Linux recompile your OS image&quot;. Yocto is not the only option in this space, but perhaps the closest embedded Linux development has to a standard option.<p>Yocto is not entirely unlike Gentoo - recipes describe components of the system, which is built from source - but with the addition of a fairly sophisticated caching scheme, where the input to a recipe is hashed and the hash used to store the outcome, which is then reused in future builds (and the cache can be shared by different builders) unless the input changes. The other key feature of Yocto is that the system is composable in layers, where upper layers add, extend or override recipes from the lower layers. Layers can and are provided by different parties (e.g. BSP layers from HW vendors or their SW partners).<p>Yocto Linux is used by projects which need to be able to guarantee that they can bootstrap their OS build, customize the build (e.g. filter out use of certain licenses or use a specific toolchain) and to manage SW supply chain (the layer idea, and then in a business contact something like RASIC for the layers).<p>To sum it up, &quot;rebuilding the entire Linux OS w/ caching&quot; is absolutely the norm in embedded dev, as is having a compiler farm doing this hooked into your CI. Edge <em>nodes</em> like developer laptops then access the CI build farm's caches to make a local build bearable, with the caveat that you don't get incremental builds at the component level this way, so usually you still want to either dev separately against an SDK (or reuse the build root) or at least keep your components small and modular enough to not make it painful.<p>Automotive, network equipment (e.g. routers), stage/production equipment (mixers and other networked A/V gear, etc.) and many other parts of the SW world work this way.<p>Hacker News is mostly exposed to web development and desktop Linux/mobile app development, which are pretty different. Indeed, perhaps the most surprising thing is how different desktop Linux development is from embedded Linux development and how little cross-pollination between these communities is taking place.<p>If you develop your for desktop Linux, the distro on your box also serves as its SDK - you just install a bunch of -dev packages from your package manager and build against the host. Or perhaps a Docker image as build env in some cases. But you generally never rebuild/bootstrap the OS, which in embedded is the primary unit of work (with mitigations such as caching).<p>One side-effect of this is that embedded systems and desktop systems tend to approach updates/OTA differently. In the package/binary-based systems, OTAs come in via the package manager. Embedded systems historically tend to go for full system image updates with again some mitigations such as binary deltas, and then A/B partition update schemes. Or a partitioning of the update content that is orthogonal to the partitioning that goes into the OS image build. Lately there's a trend for seperating applications out into container images that get deployed seperately from the base OS image, and thoughts about containers that can move between embedded devices on the edge and the cloud infra in the back.",
                      matchLevel: 'full',
                      matchedWords: ['nodejs'],
                      fullyHighlighted: false,
                    },
                  },
                  created_at: '2022-03-28T13:06:00.000Z',
                  story_id: 30827210,
                  story_title: 'Fuchsia Workstation',
                  story_url:
                    'https://fuchsia.dev/fuchsia-src/development/build/build_workstation',
                  parent_id: 30828633,
                  created_at_i: 1648472760,
                  author_id: 2,
                  Author: {
                    id: 2,
                    username: 'sho_hn',
                    match_level: 'none',
                    matched_words: [],
                  },
                },
              ],
              totalPages: 1,
            }),
            removeOne: jest.fn().mockReturnValue({
              message: 'Article with id: 1 was deleted successfully.',
            }),
            removeMany: jest.fn().mockReturnValue({
              message: '1 articles were removed successfully.',
            }),
            retrieveAllPages: jest.fn().mockReturnValue({
              hits: [
                {
                  created_at: '2022-03-28T22:41:46.000Z',
                  title: null,
                  url: null,
                  author: 'AtlasBarfed',
                  points: null,
                  story_text: null,
                  comment_text:
                    'What are you using to scale the document storage across nodes? Is it the usual master-slave stuff?',
                  num_comments: null,
                  story_id: 30835444,
                  story_title:
                    'Writing a document database from scratch in Go: Lucene-like filters and indexes',
                  story_url: 'https://notes.eatonphil.com/documentdb.html',
                  parent_id: 30835444,
                  created_at_i: 1648507306,
                  _tags: ['comment', 'author_AtlasBarfed', 'story_30835444'],
                  objectID: '30836474',
                  _highlightResult: {
                    author: {
                      value: 'AtlasBarfed',
                      matchLeve: 'none',
                      matchedWords: [],
                    },
                    comment_text: {
                      value:
                        'What are you using to scale the document storage across <em>nodes</em>? Is it the usual master-slave stuff?',
                      matchLevel: 'full',
                      fullyHighlighted: false,
                      matchedWords: ['nodejs'],
                    },
                    story_title: {
                      value:
                        'Writing a document database from scratch in Go: Lucene-like filters and indexes',
                      matchLeve: 'none',
                      matchedWord: [],
                    },
                    story_url: {
                      value: 'https://notes.eatonphil.com/documentdb.html',
                      matchLevel: 'none',
                      matchedWord: [],
                    },
                  },
                },
              ],
              nbHit: 22537,
              page: 0,
              nbPages: 50,
              hitsPerPage: 20,
              exhaustiveNbHits: true,
              exhaustiveTypo: true,
              query: 'nodejs',
              params:
                'advancedSyntax=true&analytics=true&analyticsTags=backend&query=nodejs',
              processingTimeMS: 10,
            }),
            createArticles: jest.fn().mockReturnValue(1),
            getNbHits: jest.fn().mockReturnValue({
              id: 8,
              nb_hits: 22537,
              created_at: '2022-03-28T23:59:02.040Z',
            }),
            fillDatabase: jest.fn().mockReturnValue({
              message: 'Database was filled successfully with 1000 records.',
            }),
          },
        },
      ],
    }).compile();

    controller = module.get<ServerController>(ServerController);
    serverService = module.get<ServerService>(ServerService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('searchArticles()', () => {
    it('should get the filtered articles', async () => {
      const response = await controller.searchArticles(
        1,
        1,
        '',
        'author_sho_hn, story_30827210',
        '',
      );
      expect(typeof response).toBe('object');
      expect(response.articles.length).toBe(1);
      expect(response.articles[0].author).toBe('sho_hn');
      expect(response.totalPages).toBe(1);
    });
  });

  describe('fillDatabase()', () => {
    it('should fill the database with fetched data from API', async () => {
      const all = await serverService.retrieveAllPages();
      const count = await serverService.createArticles(all);

      const response = await controller.fillDatabase(res);
      expect(response).toBeDefined();
      expect(typeof response).toBe('object');
      expect(typeof all).toBe('object');
      expect(typeof count).toBe('number');
    });
  });

  describe('removeOne()', () => {
    it('should remove one article by id', async () => {
      const removed = await controller.removeOne(1, res);
      expect(typeof removed).toBe('object');
      expect(removed).toBeDefined();
    });
  });

  describe('removeMany()', () => {
    it('should remove many articles by author name', async () => {
      const removed = await controller.removeMany('ayoisaiah', res);
      expect(removed).toBeDefined();
      expect(typeof removed).toBe('object');
    });
  });

  describe('getLastNbHit()', () => {
    it('should get last nb hits', async () => {
      const nbHit = await controller.getLastNbHit();
      expect(typeof nbHit).toBe('object');
      expect(nbHit.nb_hits).toEqual(22537);
    });
  });
});
