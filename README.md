
# Junior Developer at Reign

This repo is a test for a Junior Developer at Reign. To pagination, I used this [doc](https://hn.algolia.com/api) as a reference.
## Installation

To install all the dependencies, please run:

```sh
npm i 
```

## Usage

For the first run, you need to create your initial Prisma setup using this init command:

```sh
npm run prisma-init
```

NOTE: This command, executes `npx prisma init` at the background. To see more, please check the [official NestJS doc](https://docs.nestjs.com/recipes/prisma#set-up-prisma).

Later, to generate Prisma's schema, please run:

```sh
npm run generate
```

To create tables in the database, run:

```sh
npm run sync-tables
```

If you already have some data in the database and you want to have a fresh one, you can run:

```sh
npm run clean-db
```

... But you should know if you do that, you have to run again this commands:

```sh
npm run generate
npm run sync-tables
```

## Running the app

To run the project, you can do this:

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

To run the test, you can do:

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Swagger

To get the API documentation, please run `http://localhost:3000/api` in your browser.
NOTE: There's something important to highlight, and it's for `/server/search` route, the only two required query params are `page` and `per_page`, but in Swagger it's shown as if all query parameters are required.

### Endpoints

There's few endpoints that can be requested, they're:

#### Search Articles by filters
`/server/search`
The possible filters are: 
- `page`: number of page
- `per_page`: elements per page
- `author`: the article's author (hit)
- `_tags`: the tags of the article (hit). This query param must be a comma-separated string, for example: `author_ScottWRobinson,story_30494644`
- `title`: the title ofthe article (hit)

#### Fill the database
`/server/fill`
This endpoint must be used to fill the database at first time, fetching all pages from the Algolia's API.

#### Remove one article by article id
`/server/remove/one/{id}`
This endpoint removes one record form the database, using the database article's id to do that.

#### Remove many article by author
`/server/remove/many/{authorName}`
This endpoint removes one or more records form the database, using the author's username to do that.

#### Get last number of articles (hits)
`/server/nbhits`
This endpoint return the lsat nbHits stored in the database.

## License

This project is [MIT licensed](LICENSE).
